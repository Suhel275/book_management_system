import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.suhel.Book;
@WebServlet("/Update2")
public class Update2Controller extends HttpServlet
{
	public void doGet(HttpServletRequest req,HttpServletResponse resp)throws ServletException,IOException
	{
		String[] s=req.getParameterValues("Update");
		ServletContext context=getServletContext();
		context.setAttribute("Temp",s);
		ArrayList<Book> al=ViewModel.update2(s);
		for(Book ob:al)
		{
			System.out.println(ob.getId()+"\t"+ob.getName()+"\t"+ob.getPrice()+"\t"+ob.getUnit());
		}
		req.setAttribute("Result",al);
		RequestDispatcher rd=req.getRequestDispatcher("Update2.jsp");
		rd.forward(req,resp);
			
			
		
		
	}
}
