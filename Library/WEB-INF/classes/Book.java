package com.suhel;
public class Book{
	private int bId;
	private String bName;
	private double bPrice;
	private int bUnit;
	public void setId(int bId)
	{
		this.bId=bId;
	}
	public int getId()
	{
		return bId;
	}
	public void setName(String bName)
	{
		this.bName=bName;
	}
	public String getName()
	{
		return bName;
	}
	public void setPrice(double bPrice)
	{
		this.bPrice=bPrice;
	}
	public double getPrice()
	{
		return bPrice;
	}
	public void setUnit(int bUnit)
	{
		this.bUnit=bUnit;
	}
	public int getUnit()
	{
		return bUnit;
	}
}