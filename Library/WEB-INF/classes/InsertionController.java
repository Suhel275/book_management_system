import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.*;
import java.sql.*;
@WebServlet("/Insertion")
public class InsertionController extends HttpServlet
{
	public void service(HttpServletRequest req,HttpServletResponse resp)throws ServletException,IOException
	{
		PrintWriter out=resp.getWriter();
		String b_name=req.getParameter("bname");
		double b_price=Double.parseDouble(req.getParameter("bprice"));
		int b_unit=Integer.parseInt(req.getParameter("bunit"));
		try
		{
		int inserted=InsertionModel.getResult(b_name,b_price,b_unit);
		if(inserted!=0)
		{
			req.setAttribute("insert","--->Record is inserted sucessfully");
			RequestDispatcher rd=req.getRequestDispatcher("Home.jsp");
			rd.forward(req,resp);
		}
		else
		{
			req.setAttribute("insert","--->Record is not inserted");
			RequestDispatcher rd=req.getRequestDispatcher("Home.jsp");
			rd.forward(req,resp);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}