import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.suhel.Book;
@WebServlet("/Update")
public class UpdateController extends HttpServlet
{
	public void doGet(HttpServletRequest req,HttpServletResponse resp)throws ServletException,IOException
	{
			List<Book> al=ViewModel.getResult();
			
			
			req.setAttribute("Result",al);
			RequestDispatcher rd=req.getRequestDispatcher("Update.jsp");
			rd.forward(req,resp);
		
		
	}
}
