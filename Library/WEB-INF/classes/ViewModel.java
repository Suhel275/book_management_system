import java.sql.*;
import java.util.*;
import com.suhel.Book;
public class ViewModel
{
	public static ArrayList getResult()
	{
		ArrayList<Book> al=new ArrayList<Book>();
		try
		{
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection con=DriverManager.getConnection("jdbc:mysql:///book","root","root");
		Statement st=con.createStatement();
		ResultSet rs=st.executeQuery("select * from book");
		while(rs.next())
		{
			Book ob=new Book();
			ob.setId(rs.getInt(1));
			ob.setName(rs.getString(2));
			ob.setPrice(rs.getDouble(3));
			ob.setUnit(rs.getInt(4));
			al.add(ob);
		}
		con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return al;
	}
	
	
	public static int delete(String[] s)
	{
		int result=0;
	try
		{
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection con=DriverManager.getConnection("jdbc:mysql:///book","root","root");
		Statement st=con.createStatement();
		for(int i=0;i<s.length;i++)
		{
	    String query=String.format("delete from book where bid=%d",Integer.parseInt(s[i]));
		result=st.executeUpdate(query);
		
		}
		con.close();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return result;
	}
	

	public static ArrayList update2(String[] s)
	{
		ArrayList<Book> al=new ArrayList<Book>();
		try
		{
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection con=DriverManager.getConnection("jdbc:mysql:///book","root","root");
		Statement st=con.createStatement();
		for(int i=0;i<s.length;i++)
		{
		String query=String.format("select * from book where bid=%d",Integer.parseInt(s[i]));
		ResultSet rs=st.executeQuery(query);
		while(rs.next())
		{
			Book ob=new Book();
			ob.setId(rs.getInt(1));
			ob.setName(rs.getString(2));
			ob.setPrice(rs.getDouble(3));
			ob.setUnit(rs.getInt(4));
			al.add(ob);
		}
		}
		
		con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return al;
	}
	
		public static int update3(ArrayList<Book> al)
	{
		int updated=0;
		try
		{
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection con=DriverManager.getConnection("jdbc:mysql:///book","root","root");
		Statement st=con.createStatement();
		for(Book ob:al)
		{
			String bname=ob.getName();
			double bprice=ob.getPrice();
			int bunit=ob.getUnit();
			int bid=ob.getId();
			String query=String.format("update  book set bname='%s' , bprice=%e , bunit=%d where bid=%d",bname,bprice,bunit,bid);
			updated=st.executeUpdate(query);
		}
		con.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
return updated;
	
	
	
	
	}
	
}